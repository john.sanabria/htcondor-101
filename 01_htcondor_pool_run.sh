#!/usr/bin/env bash
. variables.sh
EXISTS=$(docker network ls | awk '{print $2}' | grep ${HTCONDOR_NET})
if [ "${EXISTS}" == "" ]; then
  echo "Creating network ${HTCONDOR_NET}"
  docker network create ${HTCONDOR_NET}
fi
docker container run -d --rm -it --name ${CM} --net ${HTCONDOR_NET} --hostname ${CM_HOSTNAME} ${CM_CONTAINER}
docker container run -d --rm -it -v $(pwd)/htcondor_examples:/htcondor_examples --name ${ACC} --net ${HTCONDOR_NET} --hostname ${ACC_HOSTNAME} ${ACC_CONTAINER}
docker container run -d --rm -it --name ${EXE} --net ${HTCONDOR_NET} --hostname ${EXE_HOSTNAME} ${EXE_CONTAINER}
