#!/usr/bin/env bash
. variables.sh
for i in ${EXE} ${ACC} ${CM}; do
  docker stop "${i}"
done
docker network rm ${HTCONDOR_NET}
