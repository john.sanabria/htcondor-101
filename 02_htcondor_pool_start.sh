#!/usr/bin/env bash
#
for i in ${CM} ${ACC} ${EXE}; do
  echo -n "Iniciando servicio de HTCondor en \"${i}\"... "
  docker container exec "${i}" sh -c condor_master
  echo " Done!"
done
